

/*--------------------------------------------------------------
  As a business user of smart multizone irrigation, I should be able to create simulated valves and sensors that can send data to my gateway. Simulatore should meet following conditions: 

Sensor Node will send the data at every 30min  - Done

The data ( Soil moisture value) will vary as per defined array. which will repeate.

Each sensor node data array will be different

Valve node will respiond to the command sent.

We should be able to desable/enable the valve node response to simulate fail safe condition

the zone value can be from 1 to  6

Message level protocol                           //Wemos_LoRa_MultizoneSNVNSimulator// 4 SN ( 11 ,12 ,13 ,14) and// 4 VN ( 11,12,13,14)The Node id of SN and VN can be hard configured in config.h file( to be created)1.The data sent by Sensor Node is

{ "pid": "SNSTS", "nid": 16, "vals": [ mid, volt ], "st": [type1, type2, type3], "sv": [val1, val2, val3 ] ,"tx": [Moisture reading interval, data Sending interval,Tx Power , Resitance difference of probe  previous and current reading] }e.g.{ "pid": "SNSTS", "nid": 16, "vals": [1, 3323 ], "st": [2, 1, 1], "sv": [786, 12, 25] ,"tx": [5, 30, 20, 10]}// all the data can be constant except// nid , mid , val1,val2,val3   for all SN.

Just after transmission of data ( For simulator, let it accept and respond any time), Sensor Node waits 5 sec for any config data sent from Gateway(Promini) which is of followingformat//new{ "pid": "SNCMD", "nid": 15, "nnid": 16, "tx": [5, 30, 20, 10], "st": [0, 1, 2] }// st = sensor type.  0-No sensor  1-WM  2-PB

VN

// nid should be corresponding to Sensor node for convinience.//( Like For Zone 1 ,  Sensor Node ID 11  , VN ID is also 11.

//{ "pid": "VNCMD", "nid": 11,"args": [1, 1] } // onOff, reset//{ "pid": "VNSTS", "nid": 11,"vals": [1,3322] }  //[ onOff: 1/0, volt: 3322]] }

// to change nid//{ "pid": "VNCMD", "nid": 11, "nnid": 11 } // change nid

// How to find the current ID of VN Node//{ "pid": "VNCMD", "nid": 999 } //  all VN Node will respond with at randomise interval in 3-5 sec//Reply from All VN node//{ "pid": "VNSTS", "nid": 10,"vals": [0,3322] }  //[ onOff: 1/0, volt: 3322]] }//{ "pid": "VNSTS", "nid": 11,"vals": [0,3322] }  //[ onOff: 1/0, volt: 3322]] }


  Program:      Sensor/Valve node simulator

  Description:  Replicates Messages as sensor nodes and valve nodes, to test the respone from main controller
                takes data from 3 different array with some other parameters and broadcast.
                it contains 4 sensor nodes and 4 valve nodes

  
  Hardware:     ESP8266 12E NodeMCU Lolin .
                Modem RFM96 - HopeRF electronics
             
                
  Software:     VS Code,PlatformIO
  
  Date:         20-06-2021

  Based: RH_RF95.h library              https://github.com/PaulStoffregen/RadioHead/tree/master/examples
          Adafruit                      https://github.com/adafruit/RadioHead/tree/master/examples

         RH_RF95.h API                  http://www.airspayce.com/mikem/arduino/RadioHead/RH__RF95_8h_source.html                             
 
--------------------------------------------------------------*/
/*
                          WEMOS PIN OUT
                                        _________
                                      /           \
                                      |           |
                              Tx (1)  |           | RST
                              Rx (3)  |           | A0        Analog input,  A0->--220K(Series)-|- 100K( to ground).
   RFM95_INT                  D1 (5)  |           | D0(16)    RFM95_RST  
                              D2 (4)  |           | D5(14)    
 10k Pull-up                  D3 (0)  |           | D6(12)    MISO 
 10k pull-up, BUILTIN_LED     D4 (2)  |           | D7(13)    MOSI
                              GND     |   ___     | D8(15)    RFM95_CS - 10k pull-down, SS
                             +5V      |  |   |    | 3.3V  
                                       -----------                  
                                          USB   

All IO have interrupt/pwm/I2C/one-wire supported(except D0)

RFM95_CS  = CS (Chip Selector or Slave Selector ).
RFM95_RST = Reset Radio when initializing.
RFM95_INT = Interruption from DIO0 output.

*/
#include <Arduino.h>
#include <EEPROM.h>
#include <SPI.h>
#include <RH_RF95.h>
#include <ArduinoJson.h>
#define RFM95_CS 15  // D8
#define RFM95_RST 16 // D0
#define RFM95_INT 5  // D1    , D4 =2

#define RF95_FREQ 868.0 //915.0

#define LED 2 // D4  =2 bulitin

RH_RF95 rf95(RFM95_CS, RFM95_INT);
int nodeid = 10; // please edit here to change node id

unsigned long previousMillis_any = 0; // will store last time LED was updated
unsigned long currentMillis_any = 0;
const long interval = 2000; // interval at which to blink (milliseconds)
unsigned long previousMillis_RF_LED = 0; // will store last time LED was updated
unsigned long currentMillis_RF_LED = 0;
unsigned long interval_RF_LED = 2000;
unsigned long currentMillis_wdt;
unsigned long previousMillis_wdt;
unsigned long previousMillis_Interval = 0; // will store last time LED was updated
int client_any;             // serial data received Flag
int client_RF;
int delayflag;
String SerialInString = "";
bool NewSerialLineRx = false;
String ThisPumpStatus;

uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];
String toEsp8266 = "";
int tx_power = 15;
int mid;
long b11, b12, b13, b14;
uint8_t a1, a2, a3, a4;
char nodectrl[8] = {'1', '1', '1', '1', '1', '1', '1', '1'};

int val1[48] = {250, 500, 1000, 1750, 2500, 3750, 5000, 6750, 7500, 8750, 10000, 11750, 12500,
                15000, 16750, 17500, 18750, 20000, 21750, 22500, 23750, 25000, 26750, 27500,
                28750, 30000, 31750, 32500, 33750, 35000, 36750, 37500, 38750, 40000, 41750,
                42500, 43750, 45000, 46750, 47500, 48750, 50000, 52500, 55000, 57500, 60000, 62500, 65000};
int val2[48] = {4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52, 56, 60, 64, 68, 72, 76, 80, 84, 88, 92, 96,
                100, 104, 108, 112, 116, 120, 124, 128, 132, 136, 140, 144, 148, 152, 156, 160, 164, 168, 172, 176, 180, 184, 188};
int val3[48] = {3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42, 45, 48, 51, 54, 57, 60, 63, 66, 69, 72,
                74, 75, 78, 81, 84, 87, 90, 93, 96, 99, 102, 105, 108, 111, 114, 117, 120, 123, 126, 129, 132, 136, 139, 142};


void valvenode(uint8_t nd, int status)
{
 // Serial.print(status);                     //outgoing { "pid": "VNSTS", "nid": 11,"vals": [1,3322] }
  status = status - 48; // while pasing status parameter it was addded by 48, ie 1 becomes 49, 2 become 48
  StaticJsonDocument<500> doc;
  doc["pid"] = "VNSTS"; //payload ID
  doc["nid"] = nd;      // node ID
  
  JsonArray data = doc.createNestedArray("vals");
  if (nd == 11 && status!=52)
    {
      data.add(status); 
      EEPROM.write(11,status);
    }
    if (nd == 11 && status==52)
    {
      data.add(EEPROM.read(11));
    }

    if (nd == 12 && status!=52)
    {
      data.add(status); 
      EEPROM.write(12,status);
    }
    if (nd == 12 && status==52)
    {
      data.add(EEPROM.read(12));
    }
    if (nd == 13 && status!=52)
    {
      data.add(status); 
      EEPROM.write(13,status);
    }
    if (nd == 13 && status==52)
    {
      data.add(EEPROM.read(13));
    }
    if (nd == 14 && status!=52)
    {
      data.add(status); 
      EEPROM.write(14,status);
    }
    if (nd == 14 && status==52)
    {
      data.add(EEPROM.read(14));
    }
  data.add(3323);
  String testString;
  serializeJson(doc, testString);
  byte sendSize = testString.length();
  sendSize = sendSize + 1;
  char sendBuf[sendSize];
  testString.toCharArray(sendBuf, sendSize);
  rf95.send((uint8_t *)sendBuf, sizeof(sendBuf));
  Serial.println(testString);
  NewSerialLineRx = false;
}
void MonitorSerialLine()
{
  while (Serial.available())
  {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    SerialInString += inChar;
    // if the incoming character is a newline, set a flag
    // so the following code can process it
    if (inChar == '\n')
    {
      NewSerialLineRx = true;
      //  Serial.print("Serial data Received:");   // to PC Terminal
      SerialInString.trim();
      Serial.println(SerialInString);

      digitalWrite(LED, LOW); //#define LED1_Serial_data_Received A3   // yellow - Serial Data received
      client_any = 1;         // serial data received flag
      currentMillis_any = millis();
      previousMillis_any = currentMillis_any;
      SerialInString.toCharArray(nodectrl, 9);
      SerialInString = "";
      for (int nodectrlsave = 0; nodectrlsave < 9; nodectrlsave++)
      {
        EEPROM.write(nodectrlsave, (int)nodectrl[nodectrlsave]);
        EEPROM.commit();
      }
    }
  }
}

void Monitor_LoRa_massage_and_send_Serially()
{
  if (rf95.available())
  {
    // Should be a message for us now
    uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];
    uint8_t len = sizeof(buf);
    if (rf95.recv(buf, &len))
    {
      String fromLoRaRx = (char *)buf; //{"nid":301,"P1":1}
      Serial.println("  Data from Master : " + fromLoRaRx);
      digitalWrite(LED, LOW); // low LED
      client_RF = 1;
      currentMillis_RF_LED = millis();
      previousMillis_RF_LED = currentMillis_RF_LED;

      byte sendSize = fromLoRaRx.length();
      sendSize = sendSize + 1;
      char sendBuf[sendSize];
      fromLoRaRx.toCharArray(sendBuf, sendSize); // convert string to char array for transmission
      if (sendBuf[10] == 'V' && sendBuf[11] == 'N')
      {
        if ((sendBuf[26] == '1' || sendBuf[27] == '9') && EEPROM.read(4) == '1')
          valvenode(11, (int)sendBuf[37]);
        if ((sendBuf[26] == '1' || sendBuf[27] == '9') && EEPROM.read(4) == '0')
          Serial.println("VN11 Diasbled");
        if ((sendBuf[26] == '2' || sendBuf[27] == '9') && EEPROM.read(5) == '1')
          valvenode(12, (int)sendBuf[37]);
        if ((sendBuf[26] == '2' || sendBuf[27] == '9') && EEPROM.read(5) == '0')
          Serial.println("VN12 Diasbled");
        if ((sendBuf[26] == '3' || sendBuf[27] == '9') && EEPROM.read(6) == '1')
          valvenode(13, (int)sendBuf[37]);
        if ((sendBuf[26] == '3' || sendBuf[27] == '9') && EEPROM.read(6) == '0')
          Serial.println("VN13 Diasbled");
        if ((sendBuf[26] == '4' || sendBuf[27] == '9') && EEPROM.read(7) == '1')
          valvenode(14, (int)sendBuf[37]);
        if ((sendBuf[26] == '4' || sendBuf[27] == '9') && EEPROM.read(7) == '0')
          Serial.println("VN14 Diasbled");
      }//{ "pid": "VNCMD", "nid": 11, "nnid": 11 }
    }
  }
}

void led_control()
{

  if (client_any) // serial data received flag to control Yellow LED
  {
    currentMillis_any = millis();
    if (currentMillis_any - previousMillis_any >= interval)
    {
      digitalWrite(LED, HIGH); // serial data received LED off
      client_any = 0;
    }
  }

  if (client_RF) // RF data received flag to control blue LED
  {
    currentMillis_RF_LED = millis();
    if (currentMillis_RF_LED - previousMillis_RF_LED >= interval_RF_LED)
    {
      digitalWrite(LED, HIGH); //#define LED2_RF_data_Received A2   // Blue - RF data received
      client_RF = 0;
    }
  } // LED blinling stop//
}

void Print_SystemInfo()
{
  String stringFilename = __FILE__; // const char compile_date[] = __DATE__ " " __TIME__;
  int lastslash = stringFilename.lastIndexOf('\\');
  stringFilename = stringFilename.substring(lastslash + 1);
  Serial.print("File name: ");
  Serial.print(stringFilename);
  Serial.print(" #  Date: ");
  Serial.print(__DATE__);
  Serial.print(" #  Time: ");
  Serial.println(__TIME__);
}

void setup()
{

  EEPROM.begin(512);

  pinMode(LED, OUTPUT);
  digitalWrite(LED, LOW);
  pinMode(RFM95_RST, OUTPUT);
  digitalWrite(RFM95_RST, HIGH);
  Serial.begin(9600);
  while (!Serial)
    continue;

  Print_SystemInfo();

  digitalWrite(RFM95_RST, LOW);
  delay(10);
  digitalWrite(RFM95_RST, HIGH);
  delay(10);

  while (!rf95.init())
  {

    Serial.println("LoRa radio init failed");

    while (1)
      ;
  }

  Serial.println("LoRa radio init OK!");

  if (!rf95.setFrequency(RF95_FREQ))
  {

    Serial.println("setFrequency failed");

    while (1)
      ;
  }

  Serial.print("Set Freq to: ");

  Serial.println(RF95_FREQ);

  // The default transmitter power is 13dBm, using PA_BOOST.
  // If you are using RFM95 / 96/97/98 modules using the transmitter pin PA_BOOST, then
  // you can set transmission powers from 5 to 23 dBm:

  rf95.setTxPower(15, false);

  if (!rf95.setFrequency(RF95_FREQ))
    Serial.println("setFrequency 868.0 failed !!!!");
  Serial.println("setFrequency  868.0  successful");
  rf95.setTxPower(tx_power, false);
  Serial.print(F("Power set to : "));
  Serial.println(tx_power);

  SerialInString = "";
  delay(1000);
  digitalWrite(LED, HIGH);
  Serial.println();
  Serial.println("SNVNSimulator Started ");
  Serial.println("send a 8 digit binary key to enable or disable nodes.(ex 11111111)");
  Serial.println("Each digit address corresponding sensors SN11,SN12,SN13,SN14,VN11,VN12,VN13,VN14");
  Serial.println("To disable a node please send '0' for their respective address");
  EEPROM.write(11,0);
  EEPROM.write(12,0);
  EEPROM.write(13,0);
  EEPROM.write(14,0);
}

void sensornode(uint8_t nd)
{
  StaticJsonDocument<500> doc;
  doc["pid"] = "SNSTS"; //payload ID
  doc["nid"] = nd;      // node ID
  JsonArray data = doc.createNestedArray("vals");
  if (nd == 11)
    data.add(++b11);
  if (nd == 12)
    data.add(++b12);
  if (nd == 13)
    data.add(++b13);
  if (nd == 14)
    data.add(++b14);
  data.add(3323);
  JsonArray data1 = doc.createNestedArray("sv");
  data1.add(2);
  data1.add(1);
  data1.add(1);
  JsonArray data2 = doc.createNestedArray("st");
  if (nd == 11)
  {
    data2.add(val1[a1]);
    data2.add(val2[a1]);
    data2.add(val3[a1]);
    a1++;
    if (a1 == 48)
      a1 = 0;
  }
  if (nd == 12)
  {
    data2.add(val1[a2]);
    data2.add(val2[a2]);
    data2.add(val3[a2]);
    a2++;
    if (a2 == 48)
      a2 = 0;
  }
  if (nd == 13)
  {
    data2.add(val1[a3]);
    data2.add(val2[a3]);
    data2.add(val3[a3]);
    a3++;
    if (a3 == 48)
      a3 = 0;
  }
  if (nd == 14)
  {
    data2.add(val1[a4]);
    data2.add(val2[a4]);
    data2.add(val3[a4]);
    a4++;
    if (a4 == 48)
      a4 = 0;
  }

  JsonArray data3 = doc.createNestedArray("tx");
  data3.add(5);
  data3.add(30);
  data3.add(20);
  data3.add(10);
  String testString;
  serializeJson(doc, testString);
  byte sendSize = testString.length();
  sendSize = sendSize + 1;
  char sendBuf[sendSize];
  testString.toCharArray(sendBuf, sendSize);
  rf95.send((uint8_t *)sendBuf, sizeof(sendBuf));
  Serial.println("Sensor Data broadcast : "+testString);
  NewSerialLineRx = false;
}

void loop()
{
  MonitorSerialLine();
  unsigned long currentMillis_Interval = millis();
  if (currentMillis_Interval - previousMillis_Interval >= 10000)
  { // set 450000 seconds
    previousMillis_Interval = currentMillis_Interval;
    nodeid++;
    if (nodeid == 15)
      nodeid = 11;
    if (nodeid == 11 && EEPROM.read(0) == '1')
      sensornode(11);
    if (nodeid == 11 && EEPROM.read(0) == '0')
      Serial.println("SN11 disabled");
    if (nodeid == 12 && EEPROM.read(1) == '1')
      sensornode(12);
    if (nodeid == 12 && EEPROM.read(1) == '0')
      Serial.println("SN12 disabled");
    if (nodeid == 13 && EEPROM.read(2) == '1')
      sensornode(13);
    if (nodeid == 13 && EEPROM.read(2) == '0')
      Serial.println("SN13 disabled");
    if (nodeid == 14 && EEPROM.read(3) == '1')
      sensornode(14);
    if (nodeid == 14 && EEPROM.read(3) == '0')
      Serial.println("SN14 disabled");
    led_control();
  }
  Monitor_LoRa_massage_and_send_Serially();
}