// Wemos based LoRa Universal WPC receiver.
// Responds all messages of WPC only
// Received {"nid":500,"P1":1}
// Reply {"nid":501,"P1":1}
// doesnot reply for any other format , just display on serial.

// To Disable LoRa Response send 0 through seria after power ON , Status is saved in EEPROM
// To Enable LoRa Response send 1 through seria after power ON 








// Arduino9x_RX
// -*- mode: C++ -*-
// Example sketch showing how to create a simple messaging client (receiver)
// with the RH_RF95 class. RH_RF95 class does not provide for addressing or
// reliability, so you should only use RH_RF95 if you do not need the higher
// level messaging abilities.
// It is designed to work with the other example Arduino9x_TX

/*--------------------------------------------------------------
  Program:      Comunication RMF96 LoRa & ESP8266 12E - Reception

  Description:  Example of LoRa Reception Communication with the RFM96 Modem with ESP8266 12E,
                set to 915.0 MHz

                Ejemplo de Comunicacion Recepcion LoRa con el Modem RFM96 con ESP8266 12E, 
                ajustado a 915.0 MHz

  
  Hardware:     ESP8266 12E NodeMCU Lolin .
                Modem RFM96 - HopeRF electronics
             
                
  Software:     Arduino IDE v1.8.3
  
  Date:         7 Mar 2018
   
  Modified or Created:       PDAControl   http://pdacontroles.com   http://pdacontrolen.com

  Complete Tutorial English:            http://pdacontrolen.com/comunication-lora-esp8266-radio-rfm95-1/  
  Tutorial Completo Español             http://pdacontroles.com/comunicacion-lora-esp8266-radio-rfm96-1/

  Based: RH_RF95.h library              https://github.com/PaulStoffregen/RadioHead/tree/master/examples
          Adafruit                      https://github.com/adafruit/RadioHead/tree/master/examples

         RH_RF95.h API                  http://www.airspayce.com/mikem/arduino/RadioHead/RH__RF95_8h_source.html                             
 
--------------------------------------------------------------*/
/*
                          WEMOS PIN OUT
                                        _________
                                      /           \
                                      |           |
                              Tx (1)  |           | RST
                              Rx (3)  |           | A0        Analog input,  A0->--220K(Series)-|- 100K( to ground).
   RFM95_INT                  D1 (5)  |           | D0(16)    RFM95_RST  
                              D2 (4)  |           | D5(14)    
 10k Pull-up                  D3 (0)  |           | D6(12)    MISO 
 10k pull-up, BUILTIN_LED     D4 (2)  |           | D7(13)    MOSI
                              GND     |   ___     | D8(15)    RFM95_CS - 10k pull-down, SS
                             +5V      |  |   |    | 3.3V  
                                       -----------                  
                                          USB   

All IO have interrupt/pwm/I2C/one-wire supported(except D0)

RFM95_CS  = CS (Chip Selector or Slave Selector ).
RFM95_RST = Reset Radio when initializing.
RFM95_INT = Interruption from DIO0 output.

*/
#include <Arduino.h>
#include <EEPROM.h>
#include <SPI.h>
#include <RH_RF95.h>
#include <ArduinoJson.h>
#define RFM95_CS 15    // D8
#define RFM95_RST 16    // D0
#define RFM95_INT 5   // D1    , D4 =2



#define RF95_FREQ 868.0   //915.0

#define LED 2  // D4  =2 bulitin

RH_RF95 rf95(RFM95_CS, RFM95_INT);

/******************************************************************************/
#define DEBUG 1        // uncomment it for debugging , 
                       // comment //#define DEBUG 1 for normal operation( serial1 with ESP)
//and use following way to print debug info through serial
//#ifndef DEBUG    // donot show in normal operation
//            Serial.print("fromEsp8266 received :");             // Send Reply String to Console
//            Serial.println(fromEsp8266);             // Send Reply String to Console
//#endif 
/******************************************************************************/

unsigned long previousMillis_any = 0;        // will store last time LED was updated
unsigned long currentMillis_any =0;
const long interval = 2000;           // interval at which to blink (milliseconds)
int client_any;  // serial data received Flag

//unsigned long previousMillis_RF = 0;        // will store last time LED was updated
//unsigned long currentMillis_RF =0;
//const long interval_RF = 2000;           // interval at which to blink (milliseconds)
int client_RF;

unsigned long previousMillis_RF_LED = 0;        // will store last time LED was updated
unsigned long currentMillis_RF_LED =0;
unsigned long interval_RF_LED =2000;
unsigned long currentMillis_wdt;
unsigned long previousMillis_wdt;

String SerialInString = "                                                 "; 
bool NewSerialLineRx = false;
String ThisPumpStatus;
// Dont put this on the stack:

uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];
String toEsp8266 = "";
int tx_power = 15;
int mid;
int EEPROM_addr_LoRa_Response = 0;
bool Disable_LoRa_Response_flag;
StaticJsonDocument<200> doc;
StaticJsonDocument<200> doc1;

void MonitorSerialLine() 
{  
    while (Serial.available()) 
    {
        // get the new byte:
        char inChar = (char)Serial.read();
        // add it to the inputString:
        SerialInString += inChar;
        // if the incoming character is a newline, set a flag
        // so the following code can process it
        if (inChar == '\n') 
        {
            NewSerialLineRx = true;
          //  Serial.print("Serial data Received:");   // to PC Terminal
            SerialInString.trim();
         //   Serial.println(SerialInString);
            digitalWrite(LED, LOW);  //#define LED1_Serial_data_Received A3   // yellow - Serial Data received
            client_any = 1;  // serial data received flag
            currentMillis_any = millis();
            previousMillis_any = currentMillis_any;
            if(SerialInString == "1")
            {
              Disable_LoRa_Response_flag = 0;
              Serial.println("Enabled_LoRa_Response.  To disable send 0 through Serial");
              EEPROM.write(EEPROM_addr_LoRa_Response, Disable_LoRa_Response_flag);
              EEPROM.commit();
            }
            else if(SerialInString == "0")
            {
              Disable_LoRa_Response_flag = 1;
              Serial.println("Disable_LoRa_Response.  To Enable send 1 through Serial");
              EEPROM.write(EEPROM_addr_LoRa_Response, Disable_LoRa_Response_flag);
              EEPROM.commit();
            }
        }
    }
}

void Monitor_LoRa_massage_and_send_Serially()
{
  if (rf95.available())
  {
        // Should be a message for us now   
        uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];
        uint8_t len = sizeof(buf);
        if (rf95.recv(buf, &len))
        {
            //Serial.print("Got LoRa Massage");
            //Serial.println((char*)buf);
            String fromLoRaRx = (char*)buf;  //{"nid":301,"P1":1}
            Serial.println("   Rx LoRa  : " + fromLoRaRx);
            
            digitalWrite(LED, LOW);  // low LED
            client_RF=1;  
            currentMillis_RF_LED = millis();
            previousMillis_RF_LED = currentMillis_RF_LED;

          if(!Disable_LoRa_Response_flag)  
          {
                // check if it is  from WPC only : // {"nid":301,"P1":1}  check for nid and P
                
                 byte sendSize = fromLoRaRx.length();
                 sendSize = sendSize + 1;        
                 char sendBuf[sendSize];
                 fromLoRaRx.toCharArray(sendBuf, sendSize); // convert string to char array for transmission
                 if(sendBuf[0] == 'S' && sendBuf[1] == 'N' && sendBuf[2] == '1')     // Check for Sensor Node SN1
                 {
                   Serial.println("Sensor node SN1"+(String)sendBuf[3]+" has sent the data");
                   Serial.print(F(" Reply LoRa message   : ")); Serial.println(sendBuf);
                   rf95.send((uint8_t *)sendBuf, sizeof(sendBuf));
                 }
                 if(sendBuf[0] == 'V' && sendBuf[1] == 'N' && sendBuf[2] == '1' && sendBuf[3] == '1')     // Check for valve node VN1
                 {
                   Serial.println("Valve node VN11 has sent the feedback");
                   Serial.print(F(" Reply LoRa message   : ")); Serial.println(sendBuf);
                   rf95.send((uint8_t *)sendBuf, sizeof(sendBuf));
                 }
          }

        }
  } 
}       

void led_control()
{

      if (client_any)      // serial data received flag to control Yellow LED
      { 
        currentMillis_any = millis();
        if (currentMillis_any - previousMillis_any >= interval) 
          {
            digitalWrite(LED, HIGH);       // serial data received LED off     
            client_any=0; 
          }
      }   

      if (client_RF)      // RF data received flag to control blue LED
      { 
        currentMillis_RF_LED = millis();
        if (currentMillis_RF_LED - previousMillis_RF_LED >= interval_RF_LED) 
          {
            digitalWrite(LED, HIGH);      //#define LED2_RF_data_Received A2   // Blue - RF data received      
            client_RF=0; 
          }
      }                   // LED blinling stop//


    
}

void Print_SystemInfo()
{        
    String stringFilename  =__FILE__;       // const char compile_date[] = __DATE__ " " __TIME__;  
    int lastslash = stringFilename.lastIndexOf('\\');
    stringFilename =stringFilename.substring(lastslash+1);
    Serial.print("File name: ");Serial.print(stringFilename);        
    Serial.print(" #  Date: ");Serial.print(__DATE__);
    Serial.print(" #  Time: ");Serial.println(__TIME__);
}

void setup() 
{

   EEPROM.begin(512);

  pinMode(LED, OUTPUT);
  digitalWrite(LED,LOW); 
  pinMode(RFM95_RST, OUTPUT);
  digitalWrite(RFM95_RST, HIGH);
  Serial.begin(9600);
  while (!Serial) continue;
  Serial.println();
  Serial.println("Serial to LoRa Gateway starting…!!!");
  Print_SystemInfo();
  
  if(EEPROM.read(EEPROM_addr_LoRa_Response) == 1 )
  {
    Disable_LoRa_Response_flag = 1 ;
    Serial.println("Disable_LoRa_Response.  To Enable send 1 through Serial");
  }
  else if(EEPROM.read(EEPROM_addr_LoRa_Response) == 0 )
  {
    Disable_LoRa_Response_flag = 0 ;
    Serial.println("LoRa_Response Enabled .  To Disable send 0 through Serial");
  }

  digitalWrite(RFM95_RST, LOW);
  delay(10);
  digitalWrite(RFM95_RST, HIGH);
  delay(10);

  while (!rf95.init()) {

    Serial.println("LoRa radio init failed");

    while (1);

  }

  Serial.println("LoRa radio init OK!");


  if (!rf95.setFrequency(RF95_FREQ)) {

    Serial.println("setFrequency failed");

    while (1);
    

  }

  Serial.print("Set Freq to: ");

  Serial.println(RF95_FREQ);

     // The default transmitter power is 13dBm, using PA_BOOST.
    // If you are using RFM95 / 96/97/98 modules using the transmitter pin PA_BOOST, then
    // you can set transmission powers from 5 to 23 dBm:

    rf95.setTxPower(15, false);

        if(!rf95.setFrequency(RF95_FREQ))     Serial.println("setFrequency 868.0 failed !!!!");
                                          Serial.println("setFrequency  868.0  successful");
        rf95.setTxPower(tx_power,false);  Serial.print(F("Power set to : ")); Serial.println(tx_power);  

       SerialInString="";
       delay(3000);
       digitalWrite(LED,HIGH); 

}

  void sensornode(uint8_t nid){
  doc["pid"] = "SMON";   //payload ID
  doc["nid"] =  nid;  // node ID
  doc["mid"] = mid++;   //Message ID
  int mR = random(10,200);
  doc["mR"] =  mR;  // Moisture level @ root sensor
  int mB = random(10,200);
  doc["mB"] = mB;   //Moisture level @ bottom Sensor
  int v = random(3000,4200);
  doc["v"] =  v;  // NOde battery voltage in millivolt
  doc["dur"] = 5;   //Duration to check the soil moisture 
  doc["Sdur"] =  30; // Duration to send the soil moisture data only if the soil moisture vaalue has changed 
  delay(3000);
}
void loraSend(){
  String testString;  
        serializeJson(doc, testString); 
         byte sendSize = testString.length();
         sendSize = sendSize + 1;        
         char sendBuf[sendSize];
         testString.toCharArray(sendBuf, sendSize);
         rf95.send((uint8_t *)sendBuf, sizeof(sendBuf));
         //serializeJson(doc, Serial);
         // Serial.println(" Rx LoRa  : " + fromLoRaRx);
           Serial.println("Master     : " + testString);
         SerialInString = "";
//         Serial.println(F("Sent LoRa message."));
//         Serial.println(F(".............."));
         NewSerialLineRx = false;
    delay(1000);
    led_control();

}
void loop(){
    for(int nid=11;nid<15;nid++){
      for(int h=0;h<20;h++){
      sensornode(nid);
      loraSend();
      }
      mid=0;
    } 
}